class Solution {
    public String reverseWords(String s) {
        StringBuilder result = new StringBuilder("");
        String words[] = s.split("\\s");
        for(String word : words ){
//             Dung reverse cua StringBuffer
            result.append(new StringBuffer(word).reverse().toString()+" ");
        }
        return result.toString().trim();
    }
}