class Solution {
    public boolean isPowerOfTwo(int n) {
      
        //Using bit operator
        //After using simple way (while(n%2 == 0) n/=2) -> Time Limit
        //Searched in solution (Discuss) => Best algor is bit operator
        // n & (n - 1): (2)0000 0010 & (1)0000 0001 = 0
        
        return (n > 0) && ((n & (n - 1)) == 0);
    }
}