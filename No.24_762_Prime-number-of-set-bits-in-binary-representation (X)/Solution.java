class Solution {
    public int countPrimeSetBits(int L, int R) {
        int bitCount = 0;
        for(int i = L; i <= R ; i++){
            int temp = 0;
            for(int n = i; n > 0; n >>= 1){
                temp += n & 1;
            }
            if(checkPrime(temp)) bitCount++;
        }
        return bitCount;
    }
    
    public boolean checkPrime(int x){
        return x == 2 || x == 3 || x == 5 || x == 7 || x == 11 || x == 13 || x == 17 || x == 19;
    }
}