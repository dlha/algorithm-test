class Solution {
    public int calPoints(String[] ops) {
        Stack<Integer> stack = new Stack<>();
        int result = 0;
        
        for(String s : ops){
            switch(s){
                case "C":
                    stack.pop();
                    break;
                case "D":
                    stack.push(stack.peek() * 2);
                    break;
                case "+":
                    int a = stack.pop();
                    int b = a + stack.peek();
                    stack.push(a);
                    stack.push(b);
                    break;
                default: stack.push(Integer.parseInt(s));
            }
        }
        
        for(int i : stack){
            result += i;
        }
        
        return result;
    }
}