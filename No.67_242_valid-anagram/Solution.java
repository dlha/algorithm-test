class Solution {
    public boolean isAnagram(String s, String t) {
        //Dùng tổng mã ASCII
        // int sum = 0;
        // for(char c : s.toCharArray()){
        //     sum+=(c - 'a' + 1);
        // }
        // for(char c : t.toCharArray()){
        //     sum-=(c - 'a' + 1);
        // }
        // return sum == 0;
        // Cách trên sai vì ac == bb?
        //Dùng mảng đánh dấu 
        int[] arr = new int[26];
        for(char c : s.toCharArray()){
            arr[c-'a']++;
        }
        for(char c : t.toCharArray()){
            arr[c-'a']--;
        }
        for(int i : arr){
            if(i!=0) return false;
        }
        
        return true;
    }
}