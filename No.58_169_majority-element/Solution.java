class Solution {
    public int majorityElement(int[] nums) {
        // int count = 0;
        // for(int num : nums){
        //     count = 0;
        //     for(int n : nums){
        //         if(num == n) count++;
        //     }
        //     if(count > nums.length/2) return num;
        // }
        // return -1; 
        
//         appears more than [n/2] time -> sort -> return n[length/2];
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }
}