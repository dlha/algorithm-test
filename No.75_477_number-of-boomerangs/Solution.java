class Solution {
    public int numberOfBoomerangs(int[][] points) {
        int result = 0;
        for(int i = 0; i < points.length; i++){
            Map<Integer, Integer> map = new HashMap<>();
            for(int j = 0; j < points.length; j++){
                if(j != i){
                    int dis = distances(points[i], points[j]);
                    result += map.getOrDefault(dis, 0) * 2;
                    map.put(dis, map.getOrDefault(dis, 0) + 1);
                }
            }
        }
        return result;
    }
    
    public int distances(int[] p1, int[] p2){
        return ((p2[0] - p1[0])*(p2[0] - p1[0])) + ((p2[1] - p1[1])*(p2[1] - p1[1]));
    }
    
// Tinh khoang cach x(x1, y1) va y(x2, y2)
//     sqrt((x2 - x1)^2 + (y2 - y1)^2)
}