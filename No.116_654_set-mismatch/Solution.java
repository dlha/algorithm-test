class Solution {
    public int[] findErrorNums(int[] nums) {
        Arrays.sort(nums);
        int dupplicate = -1, missing = 1;
        for(int i = 1; i < nums.length; i++){
            if(nums[i] == nums[i - 1]){
                dupplicate = nums[i];
            } else if(nums[i] > nums[i - 1] + 1) //Tang lien tiep tu 1 -> n
            {
                missing = nums[i - 1] + 1;
            }
        }
        
        int[] result = new int[2];
        result[0] = dupplicate;
        if(nums[nums.length - 1] == nums.length) {
            result[1] = missing;
        } else result[1] = nums.length;
        
        return result;
    }
}