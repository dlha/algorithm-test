class Solution {
    public void reverseString(char[] s) {
        int n = s.length;
        for(int i = 0; i * 2 < s.length; i++){
            char temp = s[i];
            s[i] = s[n - 1 - i];
            s[n - 1 - i] = temp;
        }
    }
}