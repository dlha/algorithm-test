class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        //Mỗi kí tự trong magazine chỉ được dùng 1 lần => có thẻ dùng mảng để đánh dấu
        //Toàn bộ letter đều là lowercase nên chỉ cần 26 (z-a)
        int arr[] = new int[26];
        for(char c : magazine.toCharArray()){
            arr[c - 'a']++;
        }
        for(char c : ransomNote.toCharArray()){
            arr[c - 'a']--;
            if(arr[c - 'a'] < 0) return false;
        }
        
        return true;
    }
}