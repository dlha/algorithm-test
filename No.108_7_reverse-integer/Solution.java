class Solution {
    public int reverse(int x) {
        long S = 0; //1534236469
        while( x != 0 ){
            int temp = x % 10;
            S = S * 10 + temp;
            x /= 10;
        }
        
        if(S > Integer.MAX_VALUE || S < Integer.MIN_VALUE ){
            return 0;
        } else return (int)S;
    }
}