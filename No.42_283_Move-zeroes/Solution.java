class Solution {
    public void moveZeroes(int[] nums) {
        //using two pointer
        int anchor = 0;
        for(int i = 0; i < nums.length; i++){
            if(nums[i]!=0){
                int temp = nums[i];
                nums[i] = nums[anchor];
                nums[anchor] = temp;
                
                anchor++;
            }
        }
    }
}