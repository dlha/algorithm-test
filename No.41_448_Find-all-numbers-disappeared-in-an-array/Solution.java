class Solution {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> result = new ArrayList<Integer>();
        for(int i = 0; i < nums.length; i++){
            int value = Math.abs(nums[i]) - 1; // Mang bat dau tu 0
            if(nums[value] > 0){
                nums[value] = - nums[value];
            }
        }
        
        for(int i = 0; i < nums.length; i++){
            if(nums[i] > 0) result.add(i+1);
        }
        
        return result;
    }
}