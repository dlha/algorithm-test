class Solution {
    public int distributeCandies(int[] candies) {
        int count = 1,  n = candies.length / 2;
        Arrays.sort(candies);
        for(int i = 0; i < candies.length - 1 ; i++){
            if( candies[i] < candies[i+1]){
                if(count < n) count++;
            } 
        }
        return count;
    }
}