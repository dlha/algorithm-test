class Solution {
    public int[][] flipAndInvertImage(int[][] A) {
        
        // int rows = A.length;
        // int cols = A[0].length;
        int n = A.length;
        
        for(int i = 0; i < n; i++){
            
            for(int j = 0; j * 2 < n ; j++){
                
                if(A[i][j] == A[i][n - 1 - j]){
                    
                    // A[i][j] ^= 1;
                    // A[i][n - 1 - j] = A[i][j];
                    A[i][j] = A[i][n-1-j] ^= 1;
                }
            } 
        }
        return A;
    }
}