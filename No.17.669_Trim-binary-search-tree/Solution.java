/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left; 
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public TreeNode trimBST(TreeNode root, int L, int R) {
        //Dùng đệ quy
        if(root == null) return root;
//         Nếu vai > R => Toàn bộ cánh trên phải của tree sẽ lớn hơn điều kiện => loại
        if(root.val > R) return trimBST(root.left, L, R);
//         Tương tự, toàn bộ cánh bên trái của tree sẽ nhỏ hơn điều kiện => loại
        if(root.val < L) return trimBST(root.right, L, R);
        
//         Nếu không có trường hợp nào xảy ra => xử lý trên cả 2 phía
        root.left = trimBST(root.left, L, R);
        root.right = trimBST(root.right, L, R);
        
        return root;
        
    }
}