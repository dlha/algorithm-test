class Solution {
    public String[] findWords(String[] words) {
        List<String> list = new ArrayList<String>();
        for(String s : words){
            if(s.toLowerCase().matches("[qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*")) 
               list.add(s);
        }
        String[] result = new String[list.size()];
        result = list.toArray(result);
        return result;
    }
}