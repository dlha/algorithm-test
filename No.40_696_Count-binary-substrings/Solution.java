class Solution {
    public int countBinarySubstrings(String s) {
//      Giải theo cách 2 trong solution
        if (s==null || s.length() == 0) return 0;
        int count = 0;
        int prevCount = 0, curCount = 1;
        for(int i = 1; i < s.length(); i++){
            if(s.charAt(i)==s.charAt(i-1)) curCount++;
            else {
                prevCount = curCount;
                curCount = 1;
            }
            
            if(prevCount >= curCount) count++;
        }
        return count;
    }
}