class Solution {
    public int islandPerimeter(int[][] grid) {
        int count = 0;
        int temp = 0;
        for(int row = 0; row < grid.length; row++){
            for(int col = 0; col < grid[0].length; col++){
                if(grid[row][col]==1){
                    count++;
//              Kiểm tra 2 bên
                    if(col > 0 && grid[row][col-1]==1) temp++;
                    if(col < grid[0].length - 1 && grid[row][col+1]==1) temp++;
//              Kiểm tra trên dưới
                    if(row > 0 && grid[row - 1][col]==1) temp++;
                    if(row < grid.length - 1 && grid[row + 1][col]==1) temp++;
                }
                
            }
        }
        return 4 * count  - temp;
    }
}