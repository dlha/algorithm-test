/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        
        if(root == null) return result;
        
        Stack<TreeNode> nodes = new Stack<TreeNode>();
        Stack<String> str = new Stack<String>();
        
        nodes.push(root);
        str.push("");
        
        while(!nodes.isEmpty()){
            
            TreeNode node = nodes.pop();
            String string = str.pop();
            
            if(node.left == null && node.right == null){
                result.add(string + node.val);
            }
            if(node.left != null){
                nodes.push(node.left);
                str.push(string + node.val + "->");
            }
            if(node.right != null){
                nodes.push(node.right);
                str.push(string + node.val + "->");
            }
        }
        
        return result;
    }
}