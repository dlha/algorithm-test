class Solution {
    public int maximumProduct(int[] nums) {
        //3 số nhân lại thành số lớn nhất -> Sort
        Arrays.sort(nums);
        
//      Tích 3 số cuối so sánh với tích 3 số đầu -> loại trường hợp n <= 6;
        int a = nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
        
//      nếu nums[0, 1] < 0 => max = nums[0] * num[1] * nums[length - 1] (max > 0);
        int b = nums[0] * nums[1] * nums[nums.length-1];
        
        if(a > b) return a;
        else return b;
    }
}