class Solution {
    public int findContentChildren(int[] g, int[] s) {
        int goal = 0;
        int index  = 0;
        
        Arrays.sort(g);
        Arrays.sort(s);
        
        for(int i = 0; index < g.length && i < s.length; i++){
            if(s[i] >= g[index]) {
                goal++;
                index++;
            }
        }
        
        return goal;
        
    }
}