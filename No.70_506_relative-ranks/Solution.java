class Solution {
    public String[] findRelativeRanks(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        
        for(int i = 0; i < nums.length; i++){
            map.put(nums[i], i);
        }
        
        int clone[] = nums.clone();
//         sap sep
        Arrays.sort(clone);
        for(int i = 0; i < nums.length / 2; i++){
            int temp = clone[i];
            clone[i] = clone[nums.length - 1 - i];
            clone[nums.length - 1 - i] = temp;
        }
        
        
        String[] result = new String[nums.length];
        for(int i = 0; i < nums.length; i++){
            switch(i){
                case 0: result[map.get(clone[i])] = "Gold Medal"; break;
                case 1: result[map.get(clone[i])] = "Silver Medal"; break;
                case 2: result[map.get(clone[i])] = "Bronze Medal"; break;
                default: result[map.get(clone[i])] = ""+(i+1); break;
            }
        }
        
        return result;
    }
}