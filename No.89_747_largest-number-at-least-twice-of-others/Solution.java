class Solution {
    public int dominantIndex(int[] nums) {
        int index = 0;
        
        for(int i = 1; i < nums.length; i++){
            if(nums[index] < nums[i]){
                index = i;
            }
        }
        System.out.println(index);
        
        for(int i = 0; i < nums.length; i++){
            if(nums[index] < nums[i] * 2 && index != i) return -1;
        }
        
        return index;
    }
}