class Solution {
    public String[] uncommonFromSentences(String A, String B) {
        Map<String, Integer> map = new HashMap<>();
        int count = 0;
        for(String word : (A + " " + B).split("\\s")){
            map.put(word,  map.getOrDefault(word, 0) + 1);
        }
        
        List<String> list =  new ArrayList<>();
        for(String word : map.keySet()){
            if(map.get(word) == 1) list.add(word);
        }
        
        String result[] =  new String[list.size()];
        result = list.toArray(result); 
        return  result;
    }
}