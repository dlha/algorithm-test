class Solution {
    public int peakIndexInMountainArray(int[] arr) {
        int index = 0;
        while(index < arr.length && arr[index] < arr[index+1]){
            index++;
        }
        return index;
    }
}