class Solution {
    public boolean isOneBitCharacter(int[] bits) {
        //10 11 0
        int count = 0;
        int index = 0;
        
        while(index < bits.length){
            if(bits[index]==1){
                count = 1;
                index+=2; //Vì chỉ có cặp 10 hoặc 11  -> index sẽ b ỏ qua phần tử tiếp theo mà tới thẳng phần thử sau đó
                    
            } else { // =0
                index++;
                count = 2;
            }
        }
        
        return count % 2 == 0;
    }
}