public class Solution {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int count = 0;
        int bit = 1;
        for(int i = 0; i < 32; i++){
            if((n & bit) != 0){ //1 AND 1 = 1;
                count++;
            }
            //Dich sang phai 1 bit
            bit <<=1;
        }
        
        return count;
    }
}