class Solution {
    public int findLengthOfLCIS(int[] nums) {
        int count = 0, result = 0;
        for(int i = 0; i < nums.length; i++){
            if(i==0 || nums[i] > nums[i-1]) {
                result = Math.max(++count, result);
            }
            else {
                count = 1;
            }
        }
        return result;
    }
}