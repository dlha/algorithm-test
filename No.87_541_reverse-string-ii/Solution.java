class Solution {
    public String reverseStr(String s, int k) {        
        char[] result = s.toCharArray();
        for(int i = 0; i < s.length(); i+=k*2){
            int right = i + k - 1;
            if(s.length() <= right){
                right = s.length() - 1;
            }
            for(int left = i; left < right; left++, right--){
                char temp = result[left];
                result[left] = result[right];
                result[right] = temp;
            }
        }
        return String.valueOf(result);
    }
}