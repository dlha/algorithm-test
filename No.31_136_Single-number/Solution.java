class Solution {
    public int singleNumber(int[] nums) {
//       A^A = 0; A^B = A;
        Set<Integer> set  = new HashSet<>();
        for(int num : nums){
            if(!set.add(num)){
                set.remove(num);
            }
        }
        return set.iterator().next();
    }
}