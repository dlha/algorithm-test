class Solution {
    public int firstUniqChar(String s) {
        //Dùng 1 mảng khác để đánh dấu số lần xuất hiện của kí tự
        //nhanh hơn dùng 2 vòng for lồng nhau;
        int[] arr = new int[26];//có tổng cộng 26 kí tự
        for(char c : s.toCharArray()){
            arr[c - 'a']++;
        }
        for(int i = 0; i < s.length(); i++){
            if(arr[s.charAt(i) - 'a']==1) return i;
        }
        return -1;
    }
}