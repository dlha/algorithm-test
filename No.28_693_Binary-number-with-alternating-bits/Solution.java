class Solution {
    public boolean hasAlternatingBits(int n) {
//      Run time error
        // long bit = Long.parseLong(Integer.toBinaryString(n));
        // while(bit > 0){
        //     if((bit%2)==((bit/10)%2)) return false;
        //     bit/=10;
        // }
        // return true;
        
        String bit = Integer.toBinaryString(n);
        for(int i = 0; i < bit.length() - 1; i++){
            if(bit.charAt(i)==bit.charAt(i+1)) return false;
        }
        return true;
    }
}