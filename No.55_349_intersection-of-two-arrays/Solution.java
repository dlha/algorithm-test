class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set =  new HashSet<Integer>();
        Set<Integer> intersectionSet =  new HashSet<Integer>();
        
        for(int n : nums1){
            set.add(n);
        }
        
        for(int n : nums2){
            if(set.contains(n)) intersectionSet.add(n);
        }
        
        int arr[] = new int[intersectionSet.size()];
        int index = 0;
        for(Integer n : intersectionSet){
            arr[index++] = (int)n;
        }
        
        return (int[])arr;
    }
}