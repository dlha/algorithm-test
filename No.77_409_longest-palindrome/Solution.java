class Solution {
    public int longestPalindrome(String s) {
        //dung hashset
        Set<Character> set = new HashSet<>();
        int result = 0;
        
        for(char c : s.toCharArray()){
            if(!set.add(c)){ //da ton tai c
                result += 2;
                set.remove(c);
            } 
        }
        
        //Kiem tra neu con phan tu trong set -> La char dung giua -> result + 1;
        if(set.size() > 0) result++;
        
        return result;
    }
}