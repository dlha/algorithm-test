/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public TreeNode sortedArrayToBST(int[] nums) {
        if(nums.length == 0) return null;

        return sort(nums, 0, nums.length - 1);
    }
    
    public TreeNode sort(int[] nums, int start, int end){
        if(start > end) return null;
        
        int midIndex = (end + start) / 2;
        TreeNode midNode = new TreeNode(nums[midIndex]);
        midNode.left = sort(nums, start, midIndex - 1);
        midNode.right = sort(nums, midIndex + 1, end);
        
        return midNode;
    }
}