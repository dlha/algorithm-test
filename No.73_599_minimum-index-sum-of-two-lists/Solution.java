class Solution {
    public String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        int min = Integer.MAX_VALUE;
        
        for(int i = 0; i < list1.length; i++){
            map.put(list1[i], i);
        }
        
        for(int i = 0; i <list2.length; i++){
            if(map.get(list2[i]) != null && map.get(list2[i]) + i <= min){
//                 Co the co nhieu ket qua giong nhau
                if(map.get(list2[i]) + i < min){
                    list.clear();
                    min = map.get(list2[i]) + i;
                }
                
                list.add(list2[i]);
                
            }
        }
        
        return list.toArray(new String[list.size()]);
    }
}