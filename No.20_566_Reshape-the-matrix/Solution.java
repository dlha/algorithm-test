class Solution {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        int rows = nums.length;
        int cols = nums[0].length;
        int result[][] = new int[r][c];
        
        
//         Kiểm tra điều kiện
        if(nums.length == 0) return nums;
        if(rows * cols != r * c) return nums;
        
        int x= 0, y = 0;
        
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                result[x][y] = nums[i][j];
                y++;
                if(y == c){
                    x++;
                    y=0;
                }
            }
        }
        return result;
    }
}