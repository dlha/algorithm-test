class Solution {
    public int titleToNumber(String s) {
        int result = 0;
/**         Z -> A = 26 
            BA = (2 * 26) + 1
            BAA = ((2 * 26) + 1) * 26 + 1
**/
        for(int i = 0; i < s.length(); i++ ){
            result *= 26;
            result += (s.charAt(i) - 'A' + 1);
        }
        return result;
    }
}