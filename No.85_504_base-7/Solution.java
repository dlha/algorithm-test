class Solution {
    public String convertToBase7(int num) {
        //Có thể dùng stack nhưng dài quá
        if(num==0) return "0";
        
        int n = num;
        StringBuilder string = new StringBuilder();
        
        while(n != 0){
            string.append(Math.abs(n % 7));
            n/=7;
        }
        
        if(num < 0){
            string.append("-");
        }
        
        return string.reverse().toString();
    }
}