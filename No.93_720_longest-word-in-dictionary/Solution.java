class Solution {
    public String longestWord(String[] words) {
        Arrays.sort(words);
        String result = "";
        HashSet<String> built = new HashSet<String>();
        for(String word : words){
            if(word.length() == 1 || built.contains(word.substring(0, word.length() - 1))){
                if(word.length() > result.length()){
                    result = word;
                }
                built.add(word);
            }
        }
        return result;
    }
}