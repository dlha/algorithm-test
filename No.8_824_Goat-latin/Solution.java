class Solution {
    final List<Character> vowel = new ArrayList<Character>(Arrays.asList('a', 'e', 'i', 'o','u', 'A', 'E', 'I', 'O','U'));
    public String toGoatLatin(String S) {
        StringBuilder result = new StringBuilder("");
        String sen[] = S.split("\\s");
        
        int i = 1;
        for(String word : sen){
            String newWord = (vowel.contains(word.charAt(0)) ? word : (word.substring(1) + word.charAt(0))) + "ma";
            for(int j = 1; j <= i; j++ ){
                newWord+="a";
            }
            i++;
            result.append(newWord + " ");
        }
        return result.toString().substring(0, result.length() - 1);
    }
}