class Solution {
    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<Integer>();
        for(int i = left; i <= right; i++){
            if(isDivisible(i)) result.add(i);
        }
        return result;
    }
    
    public boolean isDivisible(int n){
        int temp = n;
        while(n!=0){
         
            if(n%10==0 || temp%(n%10)!=0 ) return false;
            n/=10;
        }
        return true;
    }
}