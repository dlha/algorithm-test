class Solution {
    final String morse[] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
    public int uniqueMorseRepresentations(String[] words) {
       StringBuilder S = new StringBuilder("");
		 HashSet<String> set = new HashSet<>();
//		 Vì morse[] có các phần tử tương ứng với thứ thự Alphabet -> Có thể dùng bảng mã ASCII để lấy thứ tự chữ cái sau đó chuyển sang morse
		 for (String string : words) {
			 S.delete(0, S.length());
			 for (char c : string.toCharArray()) {
				S.append(morse[(int)c - 97]);
			}
             set.add(S.toString());
			 
		 }
		 
		 return set.size(); 
    }
}