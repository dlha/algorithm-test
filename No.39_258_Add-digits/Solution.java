class Solution {
    public int addDigits(int num) {
//         Can use Degital root but have'n known it yet :D
        if(num < 10) return num;
        int result = 0;
        do {
            while(num!=0){
                result += num % 10;
                num /= 10;
            }
            if(result < 10) return result;
            else {
                num = result;
                result = 0;
            }
        } while (true);
    }
}