class Solution {
    public boolean isPowerOfFour(int n) {
        //Solve without loops
        return (Math.log(n) / Math.log(4)) % 1 == 0;
    }
}