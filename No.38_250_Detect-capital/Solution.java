class Solution {
    public boolean detectCapitalUse(String word) {
//         All letters in this word are capitals
        if(word.toUpperCase().equals(word)) return true;
        
//         All letters in this word are not capitals
        if(word.toLowerCase().equals(word)) return true;
        
//         Onlu first letter in this word is captitals 
        // Using substring
        if(word.substring(1).toLowerCase().equals(word.substring(1))) return true;
        
        return false;
    }
}