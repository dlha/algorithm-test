class Solution {
    public int romanToInt(String s) {
        if(s.isEmpty()) return 0;
        
        Map<Character, Integer> roman = new HashMap<Character, Integer>();
        roman.put('I', 1);
        roman.put('V', 5);
        roman.put('X', 10);
        roman.put('L', 50);
        roman.put('C', 100);
        roman.put('D', 500);
        roman.put('M', 1000);
        
        int total = 0;
        
        int curPointer = 0;
        int nextPointer = 0;
        
        for(int i = 0; i < s.length(); i++){
            curPointer = roman.get(s.charAt(i));
            if(i < s.length() - 1){
                nextPointer = roman.get(s.charAt(i+1));
            } else nextPointer = 0;
            
            if(curPointer >= nextPointer) total += curPointer;
            else {
                total = total + nextPointer - curPointer;
                i++;
            }
        }
        
        return total;
    }
}