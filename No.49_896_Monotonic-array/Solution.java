class Solution {
    public boolean isMonotonic(int[] A) {
        int inc = 1;
        int dec = 1;
        for(int i = 0; i < A.length - 1; i++){
            if(A[i] >= A[i+1]) dec++;
            if(A[i] <= A[i+1]) inc++;
        }
        
        return inc == A.length || dec == A.length;
    }
}