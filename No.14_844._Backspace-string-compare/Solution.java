class Solution {
    public boolean backspaceCompare(String S, String T) {
        return getStack(S).equals(getStack(T));
    }
    
    public Stack<Character> getStack(String S){
        StringBuilder result = new StringBuilder("");
        Stack<Character> stack = new Stack();
        
        for(Character c : S.toCharArray()){
            if(c!='#'){
                stack.push(c);
            } else if(!stack.isEmpty()){
                stack.pop();
            }
        }
        return stack;
    }
}