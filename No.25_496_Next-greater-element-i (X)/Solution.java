class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] result = new int[nums1.length];
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for(int i = 0; i < nums2.length; i++){
            map.put(nums2[i], i);
        }
        
        for(int i = 0; i < nums1.length; i++){
            int index = map.get(nums1[i]);
            boolean check = false;
            for( ; index < nums2.length; index ++){
                if(nums2[index] > nums1[i]){
                    check = true;
                    result[i] = nums2[index]; 
                    break;
                }
            }
            
            if(check == false){
                result[i] = -1;
            }
        }
        
        return result;
    }
}