class Solution {
    public int[] plusOne(int[] digits) {
        
        for(int i = digits.length - 1; i >= 0; i--){
            if(digits[i] < 9){ //So cuoi cung < 9 => Cong bth
                digits[i]++;
                return digits;
            }
            
            // 999 + 1 = 1000;
            
            digits[i] = 0; //=> set tat ca cac so = 0 (length + 1)
        }
        
        int result[] = new int[digits.length + 1];
        result[0] = 1;
        
        return result;
    }
}