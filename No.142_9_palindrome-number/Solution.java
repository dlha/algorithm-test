class Solution {
    public boolean isPalindrome(int x) {
        if(x < 0) return false;
        if(x < 10) return true;
        int temp = x % 10;
        int num = x / 10;
        
        while(num > 0){
            temp = temp * 10 +  num % 10;
            
            num /= 10;
        }
        
        return temp == x;
    }
}