class Solution {
    public List<String> fizzBuzz(int n) {
        List<String> result = new ArrayList<String>();
        String s = "";
        for(int i = 1; i <= n; i++){
            if(isDivisibleBy3(i) && isDivisibleBy5(i)){
                s = "FizzBuzz";
            } else if (isDivisibleBy3(i)){
                s = "Fizz";
            } else if(isDivisibleBy5(i)){
                s = "Buzz";
            } else s = String.valueOf(i);
            
            result.add(s);
        }
        return result;
    }
    
    public boolean isDivisibleBy3(int num){
        return (num % 3 == 0);
    }
    
    public boolean isDivisibleBy5(int num){
        return (num % 5 == 0);
    }
}