class Solution {
    public char findTheDifference(String s, String t) {
        //Dùng tổng mã ASCII
        int sumCode = 0;

        for(char c : t.toCharArray()){
            sumCode += (int)c;
        }
        
        for(char c : s.toCharArray()){
            sumCode -= (int)c;
        }
        
        return (char)sumCode;
        
    }
}