class Solution {
//  Đếm số bit khác của khoảng cách Hamming 
    public int oneBitCount(int i){
        int count = 0;
        while(i > 0){
            count += i%2; //0 hoặc 1
            i >>= 1;
        }
        return count;
    }
    
    public int hammingDistance(int x, int y) {
//         Dùng toán tử dịch bit?
        int xor = x ^ y;
        return oneBitCount(xor);
    }
    
}