class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int result = 0;
        int max = 0;
        for(int i = 0; i < nums.length ; i++){
            if(nums[i] == 1){
  
                result = Math.max(result, ++max);
            } else max = 0;
        }
        return result;
    }
}